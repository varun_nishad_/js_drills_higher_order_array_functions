let arr = []
function flatten(elements) {
   for(let i=0; i<elements.length; i++){
     if(Array.isArray(elements[i])){
       flatten(elements[i]);
     }
     else arr.push(elements[i]);
   }
  return arr;
}

export { arr,flatten }


function cbFun(stVal, i){
    return stVal+i;
}

function reduce(elements, cb, startingValue) {
    let val;
    val = cb(startingValue, elements[0])
    elements.splice(0,1,val)

    for(let i=0; i<elements.length; i++){
        startingValue = elements[0]
        val = cb(startingValue, elements[i+1])
        elements.splice(0,2,val)
        i = -1
      if(elements.length == 1){
          return Number(elements.join())
      }
    }
}

export { cbFun, reduce }


function cbFun(elm){
  return elm%2 !== 0;
}

function filter(elements, cb) {
let res;
 for(let i=0; i<elements.length; i++){
    res = cb(elements[i])
    if(!res){
      elements.splice(i,1);
      i -= 1
    }
 }
return elements
}

export { cbFun, filter }

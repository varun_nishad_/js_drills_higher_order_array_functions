
function cbFun(elm , val = 4){
    return val === elm;
}

function find(elements, cb) {
    let res;
    for(let i=0; i<elements.length; i++){
        res = cb(elements[i])
        if(res) return elements[i]
    }
  return undefined
}

export { cbFun, find }

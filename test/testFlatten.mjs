import { arr,flatten } from '../flatten.mjs'

const  nestedArray = [1, [2], [[3]], [[[4]]]];
const result = flatten(nestedArray)

console.log(result)

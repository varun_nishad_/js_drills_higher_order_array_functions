import { cbFun, reduce } from '../reduce.mjs'

const items = [1,2,3,4,5,5]
const result = reduce(items, cbFun, 0)
console.log(result)
